﻿select
i.invoice_number,
i.order_number,
i.invoice_date,
i.customer_number,
i.billto_name,
round(sum(il.qty_billed * il.net_unit_price), 2) as total

from
invoice as i
join invoiceline as il on il.invoice_number = i.invoice_number
join customer as c on c.customer_number = i.customer_number
join custchar as ch on ch.customer_number = i.customer_number AND ch.characteristic = 'Salesforce Account ID'

where
i.invoice_date = '2014-09-11'
and
(c.customer_type = 'Pro'
or
c.customer_type = 'Pro Vet'
or
c.customer_type = 'Pro - OL'
or
c.customer_type = 'NPD - Consumer'
or
c.customer_type = 'NPD - Consumer - OL'
or
c.customer_type = 'Pro Distr.')
--and
--ch.characteristic = 'Salesforce Account ID'

group by
i.invoice_number,
i.order_number,
i.invoice_date,
i.customer_number,
i.billto_name

order by
i.customer_number