﻿select
ch.value AS Salesforce_Account_ID,
i.customer_number as customer_number,
round(sum(il.qty_billed * il.net_unit_price), 2) as sales_total_for_2014

from
customer as c
join custchar as ch on ch.customer_number = c.customer_number AND ch.characteristic = 'Salesforce Account ID'
join invoice as i on i.customer_number = c.customer_number
join invoiceline as il on il.invoice_number = i.invoice_number
where
(c.customer_type = 'Pro'
or
c.customer_type = 'Pro - OL'
or
c.customer_type = 'NPD - Consumer'
or
c.customer_type = 'NPD - Consumer - OL'
or
c.customer_type = 'Pro Distr.'
or
c.customer_type = 'Pro Vet'
)
AND
i.invoice_date between '2014-01-01' AND '2014-12-31'
group by
i.customer_number,
ch.value
;
