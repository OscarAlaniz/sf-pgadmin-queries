﻿SELECT
cust_number AS "Cust #",
cust_name AS "Cust Name",
custtype_code,  
shipto_name AS "Shipto Name",
salesrep_name AS "Sales Rep",
invchead_invcdate AS "Invc Date",
invchead_invcnumber AS "Invc #",
invcitem_id AS "xTuple Invoice Item ID",
item_number AS "Item Number",
item_descrip1 AS "Item Description",
round((invcitem_billed),0) AS "Qty",
invcitem_custprice,
invcitem_price AS "Price",
CASE
WHEN
invcitem_price = 0::numeric THEN '100'::text
WHEN
invcitem_custprice = 0::numeric THEN 'N/A'::text
ELSE
round((1::numeric - invcitem_price / invcitem_custprice) * 100::numeric, 0)::text
END AS discount_pct_from_list,
round(invcitem_billed * invcitem_price,2) as invoice_extended_price,
charass_value as Salesforce_ID

FROM
custinfo
join custtype on custtype_id = cust_custtype_id
join invchead on invchead_cust_id = cust_id
join invcitem on invcitem_invchead_id = invchead_id
LEFT JOIN shiptoinfo ON invchead_shipto_id = shipto_id
join salesrep on salesrep_id = cust_salesrep_id
join item on item_id = invcitem_item_id
join charass on charass_target_id = cust_id and charass_target_type = 'C'
join char on charass_char_id = char_id
      
WHERE

(custtype_code = 'Pro'
OR
custtype_code = 'Pro - OL'
OR
custtype_code = 'Pro Vet'
OR
custtype_code = 'NPD - Consumer'
OR
custtype_code = 'NPD - Consumer - OL'
OR
custtype_code = 'Pro Distr.')

AND
cust_active
AND
invchead_invcdate = '2014-09-11'
AND
char_name = 'Salesforce Account ID'

ORDER BY
invchead_invcdate ASC
;

