﻿select
i.customer_number,
i.invoice_date

from
api.invoice as i
join api.invoiceline as il on il.invoice_number = i.invoice_number
join api.customer as c on c.customer_number = i.customer_number
join api.custchar as ch on ch.customer_number = i.customer_number and ch.characteristic = 'Salesforce Account ID'

where
i.invoice_date = '2014-09-11'
and
(c.customer_type = 'Pro'
or
c.customer_type = 'Pro Vet'
or
c.customer_type = 'Pro - OL'
or
c.customer_type = 'NPD - Consumer'
or
c.customer_type = 'NPD - Consumer - OL'
or
c.customer_type = 'Pro Distr.')

group by 
i.customer_number,
i.invoice_date

order by
i.customer_number ASC
;
